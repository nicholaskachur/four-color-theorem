# Four Color Theorem Solver
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![coverage](http://gitlab.com/caosuna/four-color-theorem/-/jobs/artifacts/master/raw/.test_results/coverage.svg?job=unittest)](http://gitlab.com/caosuna/four-color-theorem/-/jobs/artifacts/master/file/.test_results/coverage_html/index.html?job=unittest)

Naive approach to solving the four-color theorem with minimal unique colors.
## Summary
This package is an attempt to solve the Four Color Theorem without knowledge of the graph structure or pattern breakdowns. It first solves the first and second neighbors of an arbitrary region. This is then repeated on another region until the graph is completed.

Process after converting image to graph:
- For each region r:
  - If unassigned:
    - Assign 0 to r
    - For each neighbor n,
      - if assigned;          pass
      - if n not neighbor to 1;        set 1
      - if n neighbor to 1 and not 2;  set 2
      - if n neighbor to 1 and 2;      set 3
      - if n neighbor to 1, 2, and 3;  raise Exception (failure)
## Usage
Call the solver on an indexed image to return a new indexed image referencing a denoted color.
```python
import fct
import numpy as np

myimage = np.load("path/to/myimg.npy")
fourcolorimage = fct.solve(myimage)
```
See the [JupyterLab notebook](notebooks/example.ipynb) for a more detailed example and assumptions.

## Dev
See [CONTRIBUTING.md](CONTRIBUTING.md)