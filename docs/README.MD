# Documentation
The documentation in this project primarily defines guides and notes for setting up a project.

## Generating the Project
1. [Initialize your project folder with git](GUIDE_GIT.MD#Steps)
2. [Update your project license text and badge](GUIDE_LICENSES.MD#Steps)
3. [Generate a Python virtual environment to work with the project.](GUIDE_VENVS.MD#Steps)
4. [Define IDE behavior](GUIDE_VSCODE.MD#Steps)
5. [Install dev tools and respective badges](GUIDE_DEVTOOLS.MD#Steps)
6. [Update file directory skeleton](GUIDE_STRUCTURE.MD#Steps)
7. [Perform editable installation](GUIDE_DEVINSTALL.MD#Steps)
8. [Define GitLab Continuous Integration](GUIDE_GITLABCI.MD#Steps)

## Maintaining the Project
1. Update YAML file with Python version(s) to be tested.
2. Update test/dev dependencies in requirements.txt
3. Update user dependencies in setup.py
4. Update the setup.py version #.
## Content

### [GUIDE_GIT.MD](GUIDE_GIT.MD)
Contains all git commands for initialization and maintenance for easy reference.

### [GUIDE_LICENSES.MD](GUIDE_LICENSES.MD)
Contains guide for defining license for project.

### [GUIDE_STEPBYSTEP.MD](GUIDE_STEPBYSTEP.MD)
Step-by-step guide for generating a Python project.

### [GUIDE_VSCODE.MD](GUIDE_VSCODE.MD)
Defines initial .vscode files