import unittest

from fct.functions import *  # pylint: disable=unused-wildcard-import


class Test_Graph(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.np = np.testing

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_ndarray_to_graph(self):
        array = np.array(
            [
                [0, 0, 2, 2, 3],
                [0, 0, 1, 2, 3],
                [0, 1, 1, 2, 3],
                [5, 5, 1, 4, 4],
                [5, 5, 4, 4, 4],
            ]
        )
        graph = ndarray_to_graph(array, verbosity=False)
        self.assertEqual(list(graph.keys()), [0, 1, 2, 3, 4, 5])
        self.assertEqual(graph[0]["neighbors"], (1, 2, 3, 5))
        self.assertEqual(graph[1]["neighbors"], (0, 2, 4, 5))
        self.assertEqual(graph[2]["neighbors"], (0, 1, 3, 4))
        self.assertEqual(graph[3]["neighbors"], (0, 2, 4))
        self.assertEqual(graph[4]["neighbors"], (1, 2, 3, 5))
        self.assertEqual(graph[5]["neighbors"], (0, 1, 4))

    def test_graph_array_to_coloredarray(self):
        array = np.array(
            [
                [0, 0, 2, 2, 3],
                [0, 0, 1, 2, 3],
                [0, 1, 1, 2, 3],
                [5, 5, 1, 4, 4],
                [5, 5, 4, 4, 4],
            ]
        )
        graph = ndarray_to_graph(array, verbosity=False)
        graph.solve()
        colored = graph_array_to_coloredarray(array, graph)
        expected = np.array(
            [
                [0, 0, 2, 2, 1],
                [0, 0, 1, 2, 1],
                [0, 1, 1, 2, 1],
                [2, 2, 1, 0, 0],
                [2, 2, 0, 0, 0],
            ]
        )
        self.np.assert_equal(expected, colored)

    def test_solve(self):
        array = np.array(
            [
                [0, 0, 2, 2, 3],
                [0, 0, 1, 2, 3],
                [0, 1, 1, 2, 3],
                [5, 5, 1, 4, 4],
                [5, 5, 4, 4, 4],
            ]
        )
        expected = np.array(
            [
                [0, 0, 2, 2, 1],
                [0, 0, 1, 2, 1],
                [0, 1, 1, 2, 1],
                [2, 2, 1, 0, 0],
                [2, 2, 0, 0, 0],
            ]
        )
        self.np.assert_equal(solve(array, verbosity=False), expected)
