import unittest

from fct.classes import *  # pylint: disable=unused-wildcard-import


class Test_Graph(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        pass

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_init(self):
        ids = (0, 1)
        neighbors = ((1,), (0,))
        graph = Graph(ids, neighbors, verbosity=False)
        self.assertEqual(graph[0]["neighbors"], (1,))
        self.assertEqual(graph[1]["neighbors"], (0,))

    def test_solve_basic(self):
        ids = (0, 1)
        neighbors = ((1,), (0,))
        graph = Graph(ids, neighbors, verbosity=False)
        graph.solve()
        self.assertEqual(graph[0]["color"], 0)
        self.assertEqual(graph[1]["color"], 1)

    def test_solve_complex(self):
        ids = range(9)
        neighbors = (
            (1, 4, 5),
            (0, 2, 3, 4),
            (1, 3, 4),
            (1, 2, 4),
            (0, 1, 2, 3, 5, 6, 7, 8),
            (0, 1, 4, 6, 7, 8),
            (4, 5, 7),
            (4, 5, 6, 8),
            (4, 5, 7),
        )
        graph = Graph(ids, neighbors, verbosity=False)
        graph.solve()
        for id in ids:
            self.assertTrue(
                graph[id]["color"] not in graph.get_neighbor_colors(id),
                f'\nRegion ({id}) has color ({graph[id]["color"]}) while its neighbors {graph[id]["neighbors"]} has colors {graph.get_neighbor_colors(id)}.',
            )

    def test_solve_failure(self):
        """
        Non-contiguous example.
        """
        ids = [0, 1, 2, 3, 4]
        neighbors = [
            [1, 2, 3, 4],
            [0, 2, 3, 4],
            [0, 1, 3, 4],
            [0, 1, 2, 4],
            [0, 1, 2, 3],
        ]
        graph = Graph(ids, neighbors, verbosity=False)
        with self.assertRaises(Exception):
            graph.solve()
