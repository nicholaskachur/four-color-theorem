import setuptools, glob

with open("README.md", "r") as fh:
    readme = fh.read()

version = "0.0.1"
author = "Carlos Osuna"
author_email = "charlie@caosuna.com"
ldct = "text/markdown"
url = "https://gitlab.com/caosuna/fct"
dependencies = ["numpy==1.18.4"]

setuptools.setup(
    name="fct",
    version=version,
    author=author,
    author_email=author_email,
    description="Four Color Theorem Solver",
    long_description=readme,
    long_description_content_type=ldct,
    url=url,
    packages=setuptools.find_packages(where="src"),
    package_dir={"": "src"},
    install_requires=dependencies,
    python_requires=">=3.7",
    scripts=glob.glob("bin/*"),
)