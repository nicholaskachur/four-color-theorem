import sys  # pragma: no cover

from . import script  # pragma: no cover


def main(*argv):
    pass


if __name__ == "__main__":  # pragma: no cover
    main(sys.argv[1:])
