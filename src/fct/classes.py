class Graph(dict):
    """
    Graph of region IDs and IDs of its neighbors.

    Usage:
        graph = Graph(ids,neighbors,verbosity=1)

    Parameters:
        ids = list of unique values identifying the graph node
        neighbors = list of lists of neighbor id values for each id
        verbosity = bool of whether to print a solution with the required number of colors

    This example describes a 3-node graph with node 0 neighboring node 1 and 2,
    node 1 neighboring node 0, node 2 neighboring node 0.
    Ex:
        ids = [0,1,2]
        neighbors = [[1,2],[0],[0]]
        graph = Graph(ids,neighbors)


    """

    def __init__(self, ids, neighbors, verbosity=1):
        super().__init__(
            {
                id: {"neighbors": tuple(neighbor), "color": None}
                for id, neighbor in zip(ids, neighbors)
            }
        )
        self.verbosity = verbosity

    def solve(self):
        regions = list(self.keys())
        while regions:
            r = regions[0]
            self[r]["color"] = 0
            regions.remove(r)
            for n in self[r]["neighbors"]:
                if self[n]["color"] is None:
                    neighbor_colors = self.get_neighbor_colors(n)
                    if 1 not in neighbor_colors:
                        self[n]["color"] = 1
                    elif 1 in neighbor_colors and 2 not in neighbor_colors:
                        self[n]["color"] = 2
                    elif (
                        1 in neighbor_colors
                        and 2 in neighbor_colors
                        and 3 not in neighbor_colors
                    ):
                        self[n]["color"] = 3
                    else:
                        raise Exception(
                            (
                                f"Region {n} cannot be assigned a color while coloring in neighbors of {r}. "
                                + f"Region {r} must not be contiguous.\n"
                            )
                        )
                    regions.remove(n)
        if self.verbosity:  # pragma: no cover
            colormax = 0
            for r in self.keys():
                colormax = max(colormax, self[r]["color"])
            print(f"Solved with {colormax+1} colors!")

    def get_neighbor_colors(self, id):
        neighbors = self[id]["neighbors"]
        colors = [self[n]["color"] for n in neighbors]
        return tuple(colors)
