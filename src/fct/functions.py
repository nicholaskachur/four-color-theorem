import numpy as np

from .classes import Graph


def ndarray_to_graph(ndarray, verbosity=True):
    ids = np.unique(ndarray)
    neighbors = []
    for id in ids:
        mask = ndarray == id
        maskwneighbors = (
            np.roll(mask, 1, 0)
            + np.roll(mask, -1, 0)
            + np.roll(mask, 1, 1)
            + np.roll(mask, -1, 1)
        )

        neighbors += [np.unique(ndarray[~mask * maskwneighbors])]
    return Graph(ids, neighbors, verbosity=verbosity)


def graph_array_to_coloredarray(ndarray, graph):
    colored = np.empty_like(ndarray, dtype=np.uint8)
    for val, info in graph.items():
        colored[ndarray == val] = info["color"]
    return colored


def solve(ndarray, verbosity=True):
    graph = ndarray_to_graph(ndarray, verbosity)
    graph.solve()
    colored = graph_array_to_coloredarray(ndarray, graph)
    return colored
